import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { authInterceptor } from './helpers/axios-interceptor';
import { errorInterceptor } from './helpers/error-interceptor';
import { LoginService } from './services/login.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'zenika_blog_front';


  constructor(private loginService: LoginService, private router: Router) {
    authInterceptor(this.loginService)
    errorInterceptor(this.loginService, this.router)

  }
}

import { User } from "./UserModel";

export interface Article {
    id: string,
    title: string,
    summary: string,
    user: User,
    content: string,
}

export interface UserDetail {
    id: string,
    username: string,
    password: string,
}

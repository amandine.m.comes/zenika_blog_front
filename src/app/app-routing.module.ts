import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticleDetailPageComponent } from './pages/article-detail-page/article-detail-page.component';
import { AuthenticationPageComponent } from './pages/authentication-page/authentication-page.component';
import { CreateArticlePageComponent } from './pages/create-article-page/create-article-page.component';
import { EditArticlePageComponent } from './pages/edit-article-page/edit-article-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { RegisterUserPageComponent } from './pages/register-user-page/register-user-page.component';
import { UserDetailPageComponent } from './pages/user-detail-page/user-detail-page.component';
import { UsersPageComponent } from './pages/users-page/users-page.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'article', component: CreateArticlePageComponent },
  { path: 'article/:id', component: ArticleDetailPageComponent },
  { path: 'edit/:id', component: EditArticlePageComponent },
  { path: 'loggin', component: AuthenticationPageComponent },
  { path: 'users/register', component: RegisterUserPageComponent },
  { path: 'users', component: UsersPageComponent },
  { path: 'users/:id', component: UserDetailPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ArticleService } from 'src/app/services/article.service';
import { LoginService } from 'src/app/services/login.service';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  constructor(public articleService: ArticleService, public loginService: LoginService, private router: Router) { }

  public search: string = '';

  onKeyupSearch(): void {
    this.articleService.fetchArticlesByTitle(this.search);
  }


  ngOnInit(): void {
    this.articleService.fetchArticles();
  }
  //appeler la méthode getOnArticle dans ngOnInit de detail

}

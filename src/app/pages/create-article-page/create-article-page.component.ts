import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { Article } from 'src/app/models/ArticleModel';
import { ArticleService } from 'src/app/services/article.service';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CreateArticleInfoDialogComponent } from 'src/app/components/create-article-info-dialog/create-article-info-dialog.component';
import { OneButonInfoDialogComponent } from 'src/app/components/one-buton-info-dialog/one-buton-info-dialog.component';


@Component({
  selector: 'app-create-article-page',
  templateUrl: './create-article-page.component.html',
  styleUrls: ['./create-article-page.component.scss']
})
export class CreateArticlePageComponent implements OnInit {

  // variables liées au formulaire
  public title: string = '';
  public summary: string = '';
  public content: string = '';



  //tableau pour le split
  public wordsTab: string[] = [];

  constructor(private articleService: ArticleService, private route: Router, public matDialog: MatDialog) { }

  onSubmitCreateArticle(): void {
    this.wordsTab = this.summary.split(" ");
    if (this.wordsTab.length <= 50) {
      this.articleService.createArticle(this.title, this.summary, this.content)
        .then((res) => {
          console.log(res);
          this.openDialogTwoButons('L\' article a bien été créé', 'Accueil');
        }).catch((res) => {
          this.openDialogTwoButons('Vous devez vous connecter', 'Abandonner la création');
        })
    } else {
      this.openDialogTwoButons('le résumé ne doit pas dépasser 50 mots', 'Abandonner la création');

    }
  }

  openDialogTwoButons(message: string, goToHomeMessage: string) {

    const dialogRef = this.matDialog.open(CreateArticleInfoDialogComponent, {
      width: '250px',
      data: { message, goToHomeMessage }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      //this.animal = result;
    });
  }


  /*
      this.articleService.createArticle(this.title, this.summary, this.content)
        .then((res) => {
           this.wordsTab = this.summary.split(" ", 50);
          
          if (this.wordsTab.length <= 50) {
            console.log(res);
            this.message = 'Article ok';
  
  
          }
        })
        .then((res) => {
          this.route.navigate(["/"]);
        })
        .catch((err) => {
          console.log(err);
          this.message = 'Le résumé ne doit pas dépasser 50 mots';
        });
        */

  ngOnInit(): void {
  }

}

import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Article } from 'src/app/models/ArticleModel';
import { UserDetail } from 'src/app/models/UserDetailModel';
import { User } from 'src/app/models/UserModel';
import { ArticleService } from 'src/app/services/article.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-detail-page',
  templateUrl: './user-detail-page.component.html',
  styleUrls: ['./user-detail-page.component.scss']
})
export class UserDetailPageComponent implements OnInit {


  public user?: UserDetail;

  constructor(private route: ActivatedRoute, public articleService: ArticleService, public userService: UserService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.articleService.getArticlesByUser(params['id']);
      this.userService.fetchUser(params['id']).then((user: any) => {
        this.user = user.data;
      })
    })
  }

}

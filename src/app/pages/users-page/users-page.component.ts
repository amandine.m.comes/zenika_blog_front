import { Component, OnInit } from '@angular/core';
import { UserDetail } from 'src/app/models/UserDetailModel';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users-page',
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.scss']
})
export class UsersPageComponent implements OnInit {

  constructor(public userService: UserService) { }

  ngOnInit(): void {
    this.userService.fetchUsers();
  }

}

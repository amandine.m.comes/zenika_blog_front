import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-register-user-page',
  templateUrl: './register-user-page.component.html',
  styleUrls: ['./register-user-page.component.scss']
})
export class RegisterUserPageComponent implements OnInit {

  constructor(private userService: UserService) { }

  // variables liées au formulaire

  public email: string = '';
  public username: string = '';
  public password: string = '';

  onSubmitCreateUser(): void {
    this.userService.createUser(this.email, this.username, this.password)
  }

  ngOnInit(): void {
  }

}

import { Component, Input, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import axios from 'axios';
import { User } from 'src/app/models/UserModel';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-authentication-page',
  templateUrl: './authentication-page.component.html',
  styleUrls: ['./authentication-page.component.scss']
})
export class AuthenticationPageComponent implements OnInit {

  @Input()
  public username: string = "";

  @Input()
  public password: string = "";

  constructor(private loginService: LoginService, private route: Router) { }

  onSubmitLogin() {
    console.log("submit");
    this.loginService.login({ username: this.username, password: this.password });
    this.loginService.verify()
      .then(() => {
        this.route.navigate(["/"]);
      })
      .catch(() => this.loginService.logout())
  }

  ngOnInit(): void {
  }

}
//gérer le nouveau fetch avec un output et eventemitter
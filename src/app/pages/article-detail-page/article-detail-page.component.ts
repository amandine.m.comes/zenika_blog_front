import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { Article } from 'src/app/models/ArticleModel';
import { UserDetail } from 'src/app/models/UserDetailModel';
import { User } from 'src/app/models/UserModel';
import { ArticleService } from 'src/app/services/article.service';
import { HomePageComponent } from '../home-page/home-page.component';

@Component({
  selector: 'app-article-detail-page',
  templateUrl: './article-detail-page.component.html',
  styleUrls: ['./article-detail-page.component.scss']
})
export class ArticleDetailPageComponent implements OnInit {

  public message: string = '';



  constructor(private activatedRoute: ActivatedRoute, public articleService: ArticleService, private route: Router) { }


  public clickOnDeleteButton() {
    this.activatedRoute.params.subscribe(params => {
      this.articleService.deleteArticle(params['id'])
        .then((res) => {
          this.route.navigate(['/']);
          this.articleService.fetchArticles();
        })
        .catch((err => {
          console.log(err);
          this.message = "Vous ne pouvez pas supprimer l'article d'un autre auteur";
        }))

    })

  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.articleService.getArticleDetail(params['id']);
    })

  }
}


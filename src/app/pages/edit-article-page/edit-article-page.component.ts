import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticleService } from 'src/app/services/article.service';
import { FormsModule } from '@angular/forms';


@Component({
  selector: 'app-edit-article-page',
  templateUrl: './edit-article-page.component.html',
  styleUrls: ['./edit-article-page.component.scss']
})
export class EditArticlePageComponent implements OnInit {

  public id: any = '';
  public content: string = '';
  public message: string = '';

  constructor(public articleService: ArticleService, private route: Router, private activatedRoute: ActivatedRoute) { }

  onSubmitEditArticle(): void {
    this.activatedRoute.params.subscribe(params => {
      this.articleService.editArticle(this.id, this.content)
        .then((res) => {
          this.route.navigate(["/"]);
          this.articleService.fetchArticles();
        })
        .catch((err) => {
          console.log(err);
          this.message = "Vous ne pouvez pas modifier l'article d'un autre auteur";
        });

    })
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id']
      this.articleService.getArticleDetail(this.id);
    })

  }
}

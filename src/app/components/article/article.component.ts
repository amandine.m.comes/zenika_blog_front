import { Component, Input, OnInit } from '@angular/core';
import { Article } from 'src/app/models/ArticleModel';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  constructor() { }

  @Input()
  public article?: Article;

  ngOnInit(): void {
  }

}

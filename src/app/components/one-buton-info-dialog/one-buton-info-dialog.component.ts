import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-one-buton-info-dialog',
  templateUrl: './one-buton-info-dialog.component.html',
  styleUrls: ['./one-buton-info-dialog.component.scss']
})
export class OneButonInfoDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }

}

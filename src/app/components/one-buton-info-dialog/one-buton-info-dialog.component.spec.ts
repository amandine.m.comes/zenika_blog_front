import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OneButonInfoDialogComponent } from './one-buton-info-dialog.component';

describe('OneButonInfoDialogComponent', () => {
  let component: OneButonInfoDialogComponent;
  let fixture: ComponentFixture<OneButonInfoDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OneButonInfoDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OneButonInfoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

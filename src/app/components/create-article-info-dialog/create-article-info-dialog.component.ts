import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-create-article-info-dialog',
  templateUrl: './create-article-info-dialog.component.html',
  styleUrls: ['./create-article-info-dialog.component.scss']
})
export class CreateArticleInfoDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateArticleInfoDialogComponent } from './create-article-info-dialog.component';

describe('CreateArticleInfoDialogComponent', () => {
  let component: CreateArticleInfoDialogComponent;
  let fixture: ComponentFixture<CreateArticleInfoDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateArticleInfoDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateArticleInfoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

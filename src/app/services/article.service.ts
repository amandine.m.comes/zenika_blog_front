import { Injectable } from '@angular/core';
import axios from 'axios';
import { Article } from '../models/ArticleModel';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor() { }

  apiUrl: String = 'http://localhost:8080/articles';


  public articles: Article[] = [];
  public article?: Article;



  public fetchArticles() {
    axios.get(`${this.apiUrl}`)
      .then((res) => {
        console.log(res.data);
        this.articles = res.data;
      })
      .catch((err => {
        console.log(err);
      }))
  }

  public fetchArticlesByTitle(title: string) {
    axios.get(`${this.apiUrl}`, {
      params: {
        title: title
      }
    })
      .then((res) => {
        console.log(res.data);
        this.articles = res.data;
      })
      .catch((err => {
        console.log(err);
      }))
  }


  public createArticle(title: String, summary: String, content: String): Promise<Article> {
    return axios.post(`${this.apiUrl}`,
      {
        title: title,
        summary: summary,
        content: content,
      }
    )/*
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      })*/
  }
  //pour authentification : header clef :authentification valeur : basic
  //passer par interceptor : get pas auto, autres verbes auto : créer un component : qui va être un axios interceptor : va enrichir la requête avec les headers

  public getArticleDetail(id: number) {
    axios.get(`${this.apiUrl}/${id}`)
      .then((res) => {
        console.log(res.data);
        this.article = res.data;
      })
      .catch((err => {
        console.log(err);
      }))
  }

  public deleteArticle(id: number): Promise<Article> {
    return axios.delete(`${this.apiUrl}/${id}`
    )
    /*
      .then((res) => {
        this.fetchArticles();
      })
      .catch((err => {
        console.log(err);
      }))
      */
  }


  public editArticle(id: number, content: String): Promise<Article> {
    return axios.put(`${this.apiUrl}/${id}`, {
      content: content
    })
    /*
      .then((res) => {
        this.fetchArticles();
      })
      .catch((err) => {
        console.log(err);
      })
      */
  }

  public getArticlesByUser(id: number) {
    console.log(id)
    axios.get(`${this.apiUrl}/user/${id}`)
      .then((res) => {
        console.log(res.data);
        this.articles = res.data;
      })
      .catch((err => {
        console.log(err);
      }))

  }
}


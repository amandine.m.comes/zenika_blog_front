import { Injectable } from '@angular/core';
import axios from 'axios';
import { UserDetail } from '../models/UserDetailModel';
import { User } from '../models/UserModel';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  apiUrl: String = 'http://localhost:8080/users';
  public users: UserDetail[] = [];
  public user?: UserDetail;

  constructor() { }

  public createUser(email: String, username: String, password: String) {
    return axios.post(`${this.apiUrl}`,
      {
        email: email,
        username: username,
        password: password
      }
    )
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      })
  }

  public fetchUsers() {
    axios.get(`${this.apiUrl}`)
      .then((res) => {
        console.log(res.data);
        this.users = res.data;
      })
      .catch((err => {
        console.log(err);
      }))
  }

  public fetchUser(id: string): Promise<UserDetail> {
    return axios.get(`${this.apiUrl}/id/${id}`);
  }
}




import { Injectable } from '@angular/core';
import axios from 'axios';
import { User } from '../models/UserModel';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private readonly SESSION_STORAGE_KEY = "currentUser";

  apiUrl: String = 'http://localhost:8080/users';

  public user?: User;

  constructor() { }

  verify(): Promise<any> {
    return axios.post(`${this.apiUrl}/me`);
  }

  isLoggedIn(): boolean {
    if (sessionStorage.getItem(this.SESSION_STORAGE_KEY)) {
      return true;
    } else {
      return false;
    }
  }

  login(user: User): void {
    sessionStorage.setItem(this.SESSION_STORAGE_KEY, JSON.stringify(user));
    console.log(user);
  }

  logout(): void {
    sessionStorage.removeItem(this.SESSION_STORAGE_KEY)
    //alert("déconnexion");
  }

  getCurrentUserBasicAuthentication(): string {
    const currentUserPlain = sessionStorage.getItem(this.SESSION_STORAGE_KEY)
    console.log(currentUserPlain);
    if (currentUserPlain) {
      const currentUser = JSON.parse(currentUserPlain)
      console.log(currentUser);
      return "Basic " + btoa(currentUser.username + ":" + currentUser.password);
    } else {
      return "";
    }
  }


  // login(user: User) {
  //   this.user = user;
  //   console.log(this.user);
  // }

  // getCurrentUserBasicAuthentication(): string {
  //   if (this.user) {
  //     return "Basic " + btoa(this.user.username + ":" + this.user.password);
  //   } else {
  //     return "";
  //   }
  // }

}

import axios, { AxiosError, AxiosRequestConfig } from 'axios';
import { LoginService } from '../services/login.service';


export const authInterceptor = (loginService: LoginService): void => {
  //console.log("configure Axios")
  // Add a request interceptor
  axios.interceptors.request.use(
    (config: AxiosRequestConfig) => {
      if (config.headers && config.method !== 'get') {
        //console.log('interceptor');
        config.headers['Authorization'] = loginService.getCurrentUserBasicAuthentication()
      }
      return config;
    },
    (error: AxiosError) => {
      console.error('ERROR:', error)
      Promise.reject(error)
    }
  )
}







import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { CreateArticlePageComponent } from './pages/create-article-page/create-article-page.component';
import { FormsModule } from '@angular/forms';
import { ArticleComponent } from './components/article/article.component';
import { ArticleDetailPageComponent } from './pages/article-detail-page/article-detail-page.component';
import { EditArticlePageComponent } from './pages/edit-article-page/edit-article-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CreateArticleInfoDialogComponent } from './components/create-article-info-dialog/create-article-info-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { AuthenticationPageComponent } from './pages/authentication-page/authentication-page.component';
import { LoginService } from './services/login.service';
import { OneButonInfoDialogComponent } from './components/one-buton-info-dialog/one-buton-info-dialog.component';
import { RegisterUserPageComponent } from './pages/register-user-page/register-user-page.component';
import { UserDetailPageComponent } from './pages/user-detail-page/user-detail-page.component';
import { UsersPageComponent } from './pages/users-page/users-page.component';
import { UserComponent } from './components/user/user.component';
import { HeaderComponent } from './components/header/header.component';





@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    CreateArticlePageComponent,
    ArticleComponent,
    ArticleDetailPageComponent,
    EditArticlePageComponent,
    CreateArticleInfoDialogComponent,
    AuthenticationPageComponent,
    OneButonInfoDialogComponent,
    RegisterUserPageComponent,
    UserDetailPageComponent,
    UsersPageComponent,
    UserComponent,
    HeaderComponent


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatDialogModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
